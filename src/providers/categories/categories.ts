import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CategoriesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriesProvider {
    url: string;

  constructor(public http: HttpClient) {
    console.log('Hello CategoriesProvider Provider');
    this.url="https://api.themoviedb.org/3/genre/movie/list?api_key=2355ee85ea07cf5c22929b7800eee7bf&language=fr-FR";
  }
  getCategories(){
      return this.http.get(this.url);
  }

}
