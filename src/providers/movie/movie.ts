import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {
  url: string;
  year: string;
  category: string;


  constructor(public http: HttpClient) {
    console.log('Hello MovieProvider Provider');
    this.url="https://api.themoviedb.org/3/discover/movie?api_key=2355ee85ea07cf5c22929b7800eee7bf&language=fr-FR&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=80"
  }

  getMoviesDiscover(category,year){
    console.log(category);
    console.log(year);
    this.category = category;
    this.year = year;
    return this.http.get("https://api.themoviedb.org/3/discover/movie?api_key=2355ee85ea07cf5c22929b7800eee7bf&language=fr-FR&sort_by=popularity.dsc&include_adult=false&include_video=false&page=1&with_genres="+this.category+"&primary_release_year="+this.year);
  }


}
