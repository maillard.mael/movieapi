import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the SearchProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SearchProvider {
  url: string;
  query: String;

  constructor(public http: HttpClient) {
    console.log('Hello SearchProvider Provider');
  }

  searchFilms(query){
    this.query = query;
    return this.http.get("https://api.themoviedb.org/3/search/movie?api_key=2355ee85ea07cf5c22929b7800eee7bf&language=fr-FR&query="+this.query+"&page=1&include_adult=false");
  }
}
