import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Observable} from "rxjs";
import {SearchProvider} from "../../providers/search/search";

@Component({
  selector: 'page-about',
  templateUrl: 'search.html'
})
export class SearchPage {
  films: Observable<any>;
  nameFilter: string;

  constructor(public navCtrl: NavController, private searchProvider: SearchProvider) {}

  getfilms(filter){
    this.films = this.searchProvider.searchFilms(filter);
    this.films
      .subscribe(data => {console.log('my data', data);})
  }

  onInput(nameFilter){
    if (nameFilter != "") {
      this.getfilms(nameFilter);
    }
  }

  onCancel(){

  }

  openFilm(film){
    this.navCtrl.push('FilmDetailPage', {film: film});
  }
}
