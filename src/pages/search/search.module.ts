import {NgModule} from "@angular/core";
import {SearchPage} from "./search";
import {IonicPage, IonicPageModule} from "ionic-angular";

@NgModule({
  declarations: [
    SearchPage
  ],
  imports: [
    IonicPageModule.forChild(SearchPage),
  ]
})
export class SearchModule {}
