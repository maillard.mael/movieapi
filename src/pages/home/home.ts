import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {Observable} from "rxjs";
import {CategoriesProvider} from "../../providers/categories/categories";
import{MovieProvider} from "../../providers/movie/movie";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  categories: Observable<any>;
  movies: Observable<any>;
  aMovies:Observable<any>;

  constructor(public navCtrl: NavController,private categoriesP: CategoriesProvider, private movieP: MovieProvider) {


  this.categories = this.categoriesP.getCategories();
  this.categories.subscribe(data => {
          console.log('my data: ', data);
      })

  }

  openDiscoverMovie(category,year){

      this.movies = this.movieP.getMoviesDiscover(category,year)
      this.movies.subscribe(data => {
          console.log('Selected films: ', data.results[Math.floor(Math.random() * 19) + 1  ]);
      })

  }


}
